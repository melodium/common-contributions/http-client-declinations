
# `http-client` declinations

This repository essentially exists to propose different read-to-use [`http-client`](https://github.com/http-rs/http-client) declinations, depending on what features flags are enabled or not.

Crates present here exists to fill missing `cargo` functionnality detailed in [this ticket](https://github.com/rust-lang/cargo/issues/1197), as well as to propose "default expurged" versions of `http-client` when it appears on different locations in dependency tree.

# Usage

Instead of depending `http-client` crate with specific features flags, use the right declination:
```
# http-client = { version = "6", default-features = false, features = [ "h1_client", "rustls" ] }
http-client-h1-rustls = { version = "6" }
```

Naming being `http-client-<client>-<tls>`, and major version following `http-client` one.

Those crates are drop-in replacements, inside dependent crate it still use `http_client` designation.


# Use case

The [Mélodium project](https://gitlab.com/melodium/melodium) needed http client that have different feature flags enabled depending on the target platform.
Reasons were multiple, but two main ones were:
1. providing target-adapted versions of encrytion ability (os-provided on windows, rustls on linux, etc.),
1. allowing full cross-compilation of the project (and OpenSSL is known to be sometimes blocking on this, needing to disable it in some cases).
